# Farm PEP Flag Events

This module sends group 'stewards' emails when certain flags are set or unset using the [Flag Module](https://www.drupal.org/project/flag).

It can work with or without the [Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify) BUT you should read the [README of the Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify/-/blob/main/README.md) to understand why!

**NB** The Message Template config that this module relies on is currently held in the [Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify).

The module is hard coded to send group and user data to a defined message template. The message template uses tokens which may be altered, BUT must retain the machine name references to fields and templates as defined by the template.

## Possible Flags

Available Flags are 

* follow
* report
* editor_promote

# Installation

Decide if you want to install alongside/after the [Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify).

**NB** There is a dependency on the [Flag Module](https://www.drupal.org/project/flag) which is probably not coded in. So make sure that is installed.

Clone this repo in `modules/custom` and enable it in the interface.

The messages sent use the 'New Follow' template (machine name: new_follow) that is found in the `Structure -> Message Templates` section of the site. If you have installed the [Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify) these should be set up for you. 

# How it works

This uses the event subscription capabilities of Drupal (not hooks) and is based off the Drupal documentation: [Subscribe to and dispatch events]( https://www.drupal.org/docs/develop/creating-modules/subscribe-to-and-dispatch-events).

# Usage

When something is 'followed', 'reported', or 'editor-promoted' an email is sent to the group stewards (taking into account their message digest preferences - see the [README of the Farm PEP Message Notify module](https://gitlab.com/cooperativeit/farmpep_message_notify/-/blob/main/README.md) to understand why!).

There's not much to see.

Testing could be done with something like Maildev to monitor the output.