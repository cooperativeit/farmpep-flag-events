<?php

//TODO: See if it can go in the FarmPEP module or stand alone?
//TODO: Maybe rename and add to the FarmPEP module package
//TODO: Tidy up all the aborted attempts in FarmPEP, and farmpep_flag_event

namespace Drupal\farmpep_flag_events\EventSubscriber;

/** This was build from an example that uses EventSubscribe to notify of config events
 *  Some of the code is left in (but commented out) as it's useful for debugging
 * This code shows that config has been updated. A good way to test that this module is
 * installed and working.
 * e.g. save the preferences on the Performance page to get a message
 */
// use Drupal\Core\Config\ConfigCrudEvent; //Config
// use Drupal\Core\Config\ConfigEvents;    //Config

//We're 'hooking' into the Event Subscribe class
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
// Everything we need for Flags
use Drupal\flag\Event\FlaggingEvent;
use Drupal\flag\Event\UnflaggingEvent;
use Drupal\flag\Event\FlagEvents;
// Everything for messages
use Drupal\message\Entity\Message;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\farmpep_flag_events\EventSubscriber
 */
class FarmpepEventsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      // ConfigEvents::SAVE => 'configSave',      //Config
      // ConfigEvents::DELETE => 'configDelete',  //Config
      FlagEvents::ENTITY_FLAGGED => 'onFlag',
      FlagEvents::ENTITY_UNFLAGGED => 'onUnflag',
    ];
  }

  /**
   * React to flagging event.
   * 
   * We want to send an email to people to let them know that content has been
   * followed
   * reported
   * promoted
   * 
   * And maybe if it has been un-followed...(not currently implemented)
   *
   * @param \Drupal\flag\Event\FlaggingEvent $event
   *   The flagging event.
  */
  public function onFlag(FlaggingEvent $event) {
    $flagging = $event->getFlagging();

    // Who flagged something?
    //$person_who_flagged = \Drupal::currentUser();
    //$person_who_flagged = $flagging->getOwner()
    // Load load the full user entity object of the current user i.e. the person that flagged. 
    // Could use \Drupal::currentUser()->id()??
    $person_who_flagged =\Drupal\user\Entity\User::load($flagging->getOwner()->id());
 
    // What got flagged?
    $entity = $flagging->getFlaggable();
    $entity_nid = $flagging->getFlaggable()->id();

    // Who needs to be notified? All Group members?
    $group = \Drupal\group\Entity\Group::load($entity_nid);
    $members = $group->getMembers();
    foreach ($members as $member) {
      $user = $member->getUser();
      $userids[] = $user->id();
    }
    //dpm($userids);
    // OR just the group Manager / person that set the group up?
    $owner = $entity->getOwnerId();
    //dpm($owner);
    //dpm($entity->uid());
    
    // Which Flag was it? // Flags could be 'follow', 'report', 'editor_promote'
    $flag = $flagging->getFlag();
    $flag_machine_name = $flag->id();
    $flag_label = $flag->label();
 
    // DEBUG
    //$entity-> toArray());
 
    //DEBUG
    // Send a message to the screen - only 'works' if the flag is NOT set to AJAX
    // Flag id follow on 386Soil Measurement by Cooperative ITdavid@cooperativeit.co.uk1308david@cooperativeit.co.uk 
    //\Drupal::messenger()->addStatus('Flag id ' . $flag_machine_name . $flag_label . ' on ' . $entity_nid . $entity->label() . ' by ' . $person_who_flagged->getDisplayName() . $person_who_flagged->getAccountName() . $person_who_flagged->id() . $person_who_flagged->getEmail() );

    foreach ($userids as $userid) {
      // Get user account details
      $account = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($userid);
      // Check to see if Email subscriptions are allowed - should return 0 or 1 if user is in the 
      // user__message_subscribe_email table
      // and null (maybe?) if user does not have an entry there
      $has_email_subscription = $account->message_subscribe_email->value;
      
      // Send message routine..
      if ($has_email_subscription) {

        // Call message notify service.
        $notifier = \Drupal::service('message_notify.sender');
        // Create a message with node author as message creator. NB This just creates it. It doesn't send it.
        //$userid = $owner; //DEBUG
        $message = Message::create(['template' => 'new_follow', 'uid' => $userid]);
        //NB the fields_* below all correspond to fields that have been added to the 'new_follow' template.
        $message->set('field_group_reference', $entity);
        $message->set('field_flag', $flag->label());
        $message->set('field_user_reference', $person_who_flagged);
        $message->save();

        // Check the users email digest settings to see if we should send immediately?
        // Get email digest value for this user - possible values are: null, "0", "message_digest:daily", "message_digest:weekly"
        $value = $account->message_digest->value;
        //dpm($value);
    
        if (isset($value)) {
          if ($value == "message_digest:weekly") {
            $notifier->send($message, [], 'message_digest:weekly');
          } elseif ($value == "message_digest:daily") {
            $notifier->send($message, [], 'message_digest:daily'); 
          } else {
            $notifier->send($message);
          }
        } else {
          $notifier->send($message);
        }
      }
    }

    // Failed attempt at sending an ajax message
      // $response = new AjaxResponse();
      // $response->addCommand(new MessageCommand('Your changes have been saved.'));
      // return $response;
    // $message = 'Match with:';
  }

 
  public function onUnflag(UnflaggingEvent $event) {
    //NB NB DO NOTHING !!
    // Currently runs on any unflaggin event
    // Needs to test what type of event it is...
    // $flagging = $event->getFlaggings();
    // $flagging = reset($flagging);
    // $entity_nid = $flagging->getFlaggable()->id();
    //DEBUG - for this to work, the flag must be set to 'normal link' in the Structure->Flag interface
    //\Drupal::messenger()->addStatus('Flag was unset ' . $entity_nid . '.');
  }

  /**
   * //Config
   * React to a config object being deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configDelete(ConfigCrudEvent $event) {
    //$config = $event->getConfig();
    //\Drupal::messenger()->addStatus('Deleted config: ' . $config->getName());
  }

  /**
   * //Config
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
  */
  // public function configSave(ConfigCrudEvent $event) {
  //   $config = $event->getConfig();
  //   $account = \Drupal::currentUser();
  //   \Drupal::messenger()->addStatus('Saved config: ' . $config->getName() . $account->getDisplayName() . $account->getAccountName() . $account->id() . $account->getEmail() );
  // }

}